# Customers 

[[Bank]]
[[Restaraunts]]

Customers in our ecosystem are very simplified compared to real life. (at least for our MVP [[GlossaryOfTerms.md]])

What's a sensible amount of customers? Somewhere between 20 to 50 for MVP?

They will receive an income that represents their job income. Probably for MVP all the income stipends should just be the same amount for every customer and be given at the same time interval? Like twice a month for example? Or, even simpler, they'll just have a daily food budget, and the daily food money refreshes each day. We can still imagine them being price sensitive and preferring a good deal over a bad deal. 

For our simple MVP, the only use case of that income will be to spend on food at the restaurants. 

In our simplified world, the customers won't cook food and eat at home, they'll always eat at a restaurant when they get hungry (sounds nice, right?!) =. Maybe they'll just get hungry 3 times per day and eat 3 meals a day at one of the restaurants. 

For choice, we may want to consider real life concepts, like restaurant costs, wait times, food quality.

There also more advanced concepts we may want to model like the Restaurant running deals, and the customer having curiousity to try different restaurants. It'd be pretty cool to implement the feature of word of mouth, for example a customer loves a restaurant, so they tell their friends about it and the friends go to that restaurant in the future. 

Another important consideration is the role of luck and indetermism. Just like in real life, we don't always make the most rational decision, and make our choices "on a whim". So to we would want to model some luck in our system. For example, maybe a restaurant has the best value at a given moment of time, that doesn't mean 100% of customers will go there. Whatever we want to call it: Luck, feeling, indeterminism, it should play some role in our system.

We want to strike a good balance between simplicity and complexity. Starting simple and adding more complexity and nuance as we go. For example, maybe we should pick the top 2 or 3 reasons that a customer will choose one restaurant and model that decision for MVP. 

Every time the customer buys food, they should get a receipt from the transaction. 

Each customer has a bank account with the bank, the financial hub of our ecosystem. They will receive their stipend to this account which emulates them getting their paycheck from their work.