# Glossary of Terms

"MVP" == "Minimum Viable Product"

In the context of our app, MVP is like the first major accomplishment of our app. We of course will have many ideas that we may want to include, but many of the ideas may be more advanced so we can put them on a future todo list. So that we may focus on the most important concepts for MVP. 