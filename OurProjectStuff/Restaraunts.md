# Restaurants

[[Bank]]
[[Customers]]

Restaurants represent the company aspect of our ecosystem for MVP

https://www.investopedia.com/ask/answers/032515/what-difference-between-variable-cost-and-fixed-cost-economics.asp

They provide services to customers and make their money by selling food. Their costs include buying the raw ingredients for the meals they make, employee costs, and they pay the small Interchange fee on the card transactions for customers. Probably not for MVP, but maybe they have to pay rent or something? 

They keep a bank account with the bank where they store their money (and maybe in the future we can have the concept of taking on loans from the bank (to expand their business) , but for now that's probably too complex)

Restaurants want to keep a high quality of food, keep the customers happy, and to make food relatively quickly. 

Restaurants and Customers will be at the center of the concept of competitiveness in our ecosystem. For example just like in real life, a restaurant can charge high rates for a meal, but they'll lose business as customers go to another restaurant that has better costs. Therefore, Restaurants are under economic pressure to be reasonable and to do a good job to keep the customer happy. 


For simple MVP, Restaurants should only serve "in person" at the premises, not doing delivery. Although delivery would be a cool feature for later. 

Restaurants should implement a standard line concept, (data structure queue) in a FIFO (First in First Out) organization. They are motivated to move quickly because customers are sensitive to time and if they have to wait a long time, they'll consider going to a different restaurant if they can get their food their much faster. 

Each customer transaction should generate a receipt for the customer, recording basic information about the transaction including what was bought, costs, time of transcation. 