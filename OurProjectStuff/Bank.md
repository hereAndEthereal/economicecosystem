# Bank

[[Customers]]
[[Restaraunts]]

The bank is the monetary hub of the ecosystem. For MVP, we should simply have 1 bank. But, we can imagine growing this to a few banks

It both offers accounts for the businesses (for now, for simplicity's sake, we only have Restaurants in our ecosystem) and customers.

To keep it simple for now, We'll consider two revenue streams for the Bank for our first project step. read more from https://www.clevergirlfinance.com/blog/how-do-banks-make-money/
1. interest on debt
2. Interchange fees

Because Interchange fees are simpler, we'll implement them first for MVP

There are other ways banks money, but we'll stick with these two for now. In our ecosystem, we want to model a bank that is trying to provide good services, not exploit customers. Therefore our bank will not charge egregious fees like Inactivity fees, Account “maintenance” fees, and Excessive withdrawal fees for example. 







