# Main ReadMe 

the notion of time... obviously we have the concept of time, but like in video games we probably want to have concept of sped up time, game time. I'm not sure if there's a library that can help us with this.
    
    and I mentioned implementing our own versions of things, but implementing our own time library would be overkill. 

    In fact, it's worth saying that libraries are fantastic, and if we find a library that suits our purposes and needs, then great, we should use it. For example, if we find a library for a fake currency, then that's perfect and saves us effort. 


## Taking a perspective 

It'd be great to be able to view a dashboard from the perspective of any of the entities In our world. For example, we should be able to view from any customer or restaurant's perspective. 

There should also be the "global view" or "ecosystem view" which doesn't take the perspective of a particular entity, but of the whole world which includes all customers and restaurants and banks. 

## APIs are everywhere 

    All the interactions between any system in our world, whether that's customer and restuarant, customer and bank, etc... should be modelled as API interactions. The API "contract" is what we code for, and the the various coding language tools should be agostic and interchangeable. For example, as long as we maintain the API Contract, it shouldn't matter whether we're using a Node backend vs. a Django backend. 

## Being Flexible and Open Minded 

Making this system can be good for our education. But we should continue to be aware of opportunities to take the skills and knowledge we are learning in making this system and pivot them to an actual, real life, business opportunity. 

For example, we can imagine that along the way, our learnings in this project spawn other ideas that could be useful for actual business in the real world. We should highly value flexibility and open mindedness. We shouldn't be married to this project, and remember that this project is an experiment and an opportunity to learn and collaborate... If we have a real business pivot, then by all means we should consider pivoting and pursuing a potentially better opportunity.

For example, if instead of making this fake world, we can find the opportunity to make a real product and help real people and businesses, then of course that's awesome and we should strongly consider doing that! But that takes real skill and knowledge and in the meantime making this fake world will be good learning and experience for us, (and hopefully, fun)

## Principles 

It's worth saying that we should value our common humanity and aspire to make software that can help us and other people as best as possible. This means valuing gaining knowledge, skill and desiring to be servants in the communities we're a part of. For example this means valuing all the participants in a system and not exploiting one party vs the other. Simply this means that a good, healthy economic system isn't one entity benefitting at the expense of the other entities (as is sadly often the case) but trying to find a system where each participant mutually benefits. And the ecosystem as a whole is healthy and benefits each participant. 

Education, skill, learning are some of the most important principles for this project (and in life in general haha). With this in mind we should take efforts to document our code well, so that future learners can easily understand the code. We should aspire that this project may become useful for people who want to learn about code and business. And we can use this project as an opportunity to learn about these concepts ourselves so that we may teach others as well. 

## Foam 

Foam and the graph style of documentation has great potential and power and we should learn and use it effectively as a way to document the knowledge of this code base. The interconnections that the Graph provides is far more powerful and flexible than traditional note taking apps. Foam is a great open source platform and if we have ideas or Pull Requests to make it better, that'd be very good to do. 
